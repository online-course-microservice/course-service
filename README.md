# Online Course - Course Service

[![ONLINECOURSE Logo](https://ik.imagekit.io/tatangdev/online-course/Asset_5__ELvljTjxona.png)](https://github.com/tatangromadhona/online-course_api-gateway)

## Online Course

ONLINECOURSE is an udemy-like online course platform built using microservices architecture.
| Structure |
| --- |
| [![ONLINECOURSE Logo](https://ik.imagekit.io/tatangdev/online-course/Asset_5_rXsSNbDR84.png)](https://github.com/tatangromadhona/online-course_api-gateway) |

## Course Service

Service order and payment that I have combined into one to reduce the complexity of the architecture that is being worked on, this service is used to storing course purchase transaction data.

This service will build using Laravel Framework.

## Another Services

| Api Gateway | Media Service | Payment Service | User Service |
| --- | --- | --- | --- |
| [![Api Gateway](https://ik.imagekit.io/tatangdev/online-course/Asset_8_bt44CQGH_z.png)](https://github.com/tatangromadhona/online-course_api-gateway) | [![Media Service](https://ik.imagekit.io/tatangdev/online-course/Asset_4_YykdDSbga.png)](https://github.com/tatangromadhona/online-course_media-service)| [![Payment Service](https://ik.imagekit.io/tatangdev/online-course/Asset_1_M1tYLXCSBX.png)](https://github.com/tatangromadhona/online-course_payment-service)| [![User Service](https://ik.imagekit.io/tatangdev/online-course/Asset_3_cn6ASO3xsi7.png)](https://github.com/tatangromadhona/online-course_user-service)
